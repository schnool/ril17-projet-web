<?php
class Class_Departement{
    
    public function __construct($id)
    {
        $this->id = $id;
        $modelUser = new Model_ModelUser();
        $this->listUser = $modelUser->getUserByDprt($id);
    }
    
    private $id;
    private $nom;
    private $listUser = array();
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return multitype:
     */
    public function getListUser()
    {
        return $this->listUser;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * @param multitype: $listUser
     */
    public function setListUser($listUser)
    {
        $this->listUser = $listUser;
        return $this;
    }

    
    
}