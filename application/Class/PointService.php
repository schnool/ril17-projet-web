<?php
class Class_PointService {
    
    public function __construct($id)
    {
        $this->id = $id;
    }
    
    private $id;
    private $titre;
    private $description;
    private $notation;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getNotation()
    {
       $notation = '';
       switch($this->notation){
            case 'good' :
                $notation = '<i class="fas fa-thumbs-up" style="color: green"></i>';
                break;
            case 'neutral' :
                $notation = '<i class="fas fa-certificate" style="color: grey"></i>';
                break;
            case 'bad' :
                $notation = '<i class="fas fa-thumbs-down" style="color: red"></i>';
                break;
       }
        return $notation;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
        return $this;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param mixed $notation
     */
    public function setNotation($notation)
    {
        $this->notation = $notation;
        return $this;
    }

    
    
}