<?php
class Class_GestDepartement {
    public function __construct()
    {
        $modelUser = new Model_ModelUser();
        $this->listeDepartement = $modelUser->getDprt();
    }
    
    private $listeDepartement = array();
    /**
     * @return multitype:
     */
    public function getListeDepartement()
    {
        return $this->listeDepartement;
    }

    /**
     * @param multitype: $listeDepartement
     */
    public function setListeDepartement($listeDepartement)
    {
        $this->listeDepartement = $listeDepartement;
    }

}