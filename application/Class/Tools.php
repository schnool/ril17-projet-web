<?php
class Class_Tools{
    
    public static function object_to_array($obj) {
        if(is_object($obj)) $obj = (array) self::dismount($obj);
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = self::object_to_array($val);
            }
        }
        else $new = $obj;
        return $new;
    }
    
    //permet de changer les private en public, pour un meuilleur conversion des object en array
    private static function dismount($object) {
        $reflectionClass = new ReflectionClass(get_class($object));
        $array = array();
        foreach ($reflectionClass->getProperties() as $property) {
            $property->setAccessible(true);
            $array[$property->getName()] = $property->getValue($object);
            $property->setAccessible(false);
        }
        return $array;
    }  
}