<?php

class Class_GestService
{
    public function __construct($listeService = null)
    {
        $modelService = new Model_ModelService();
        
        $this->listeService = isset($listeService) ? $listeService : $modelService->getAllServices();
    }
    
    private $listeService = array();

    /**
     *
     * @return multitype:
     */
    public function getListeService()
    {
        return $this->listeService;
    }

    /**
     *
     * @param multitype: $listeService
     */
    public function setListeService($listeService)
    {
        $this->listeService = $listeService;
    }
       
    public function getInitialService(){
        $result = array();
        foreach($this->listeService as $service){
            $result[] = strtoupper($service->getNom()[0]);
        }
        return array_unique($result);        
    }
}