<?php

class Class_User
{

    public function __construct($id)
    {
        $this->id = $id;        
    }

    private $id;

    private $nom;

    private $prenom;

    private $role;

    private $departement;

    private $dateNaissance;

    private $dateDerniereConnection;

    private $pseudo;       

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @param mixed $departement
     */
    public function setDepartement($departement)
    {
        $this->departement = $departement;
        return $this;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
        return $this;
    }

    /**
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     *
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     *
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     *
     * @return mixed
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     *
     * @return mixed
     */
    public function getDateNaissance()
    {
        return date('d/m/Y', strtotime($this->dateNaissance));
    }

    /**
     *
     * @return mixed
     */
    public function getDateDerniereConnection()
    {
        return date('d/m/Y H:i:s', strtotime($this->dateDerniereConnection));
    }

    /**
     *
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     *
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     *
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
        return $this;
    }

    /**
     *
     * @param mixed $dateNaissance
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;
        return $this;
    }

    /**
     *
     * @param mixed $dateDerniereConnection
     */
    public function setDateDerniereConnection($dateDerniereConnection)
    {
        $this->dateDerniereConnection = $dateDerniereConnection;
        return $this;
    }
    
    public function getNavItem($role){
        $controller = $_SESSION['CONTROLLER'];
        switch($role){
            case 'ROLE_USER':
                $navItems = array(
                    array(
                        "href" => "/ril17-projet-web/Service",
                        "active" => $controller == 'Service' ? 'active' : '',
                        "libelle" => "Services"
                    ),
                    array(
                        "href" => "/ril17-projet-web/User",
                        "active" => $controller == 'User' ? 'active' : '',
                        "libelle" => "Profil"
                    )
                );   
            break;
            case 'ROLE_ADMIN' :
                $navItems = array(
                    array(
                        "href" => "/ril17-projet-web/Admin",
                        "active" => $controller == 'Admin' ? 'active' : '',
                        "libelle" => "Administration"
                    ),
                    array(
                        "href" => "/ril17-projet-web/Service",
                        "active" => $controller == 'Service' ? 'active' : '',
                        "libelle" => "Services"
                    ),
                    array(
                        "href" => "/ril17-projet-web/User",
                        "active" => $controller == 'User' ? 'active' : '',
                        "libelle" => "Profil"
                    )
                );
            break;
        }
        return $navItems;
    }
    
    public function getNbMentionLues(){
        $modelService = new Model_ModelService();
        $nbMention = $modelService->countServicesReadByUser($this->id);
         
        return $nbMention;
    }
    
    public function getMentionRead(){
        $modelService = new Model_ModelService();
        return $modelService->getServiceReadByUser();
    }
}