<?php

class Controller_AdminController
{

    public function __construct()
    {}

    public function indexAction()
    {
        $return = array(); 
        
        $model = new Model_ModelService();
        $model->translateToFrench('hello');
        
        $dprt = new Class_GestDepartement();
        $service = new Class_GestService();
        $serviceRead = new Class_GestServiceRead();
        
        $return['content'] = View_AdminView::renderView($dprt, $service, $serviceRead);

        return $return;
    }
    
}