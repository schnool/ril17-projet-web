<?php

class Controller_ServiceController
{
    public function __construct()
    {}

    public function indexAction()
    {
      $serviceModel = new Model_ModelService();
      
      $services = $serviceModel->getAllServices();
      $serviceCount = $serviceModel->countServices();

      if($serviceCount <= 0) {
        $apiServices = $this->getServicesFromApi();
        $this->insertServices($apiServices);
      }
      $return['content'] = View_ServiceView::renderView($services);

      
      return $return;
    }

  private function insertServices($services) {
    $serviceModel = new Model_ModelService();

    foreach ($services as $service) {
      if(isset($service['name'])) {
        if(isset($service['logo'])){
          $serviceId = $serviceModel->insertService($service['name'], $service['logo']);
         
        }
        else{
          $serviceId = $serviceModel->insertService($service['name'], null);
        }
        if(isset($service['points'])) {
          foreach ($service['points'] as $point) {
            $serviceModel->insertPoint($serviceId, $point);
          
          }
        }
     }
    } 
  }

    private function getServicesFromApi() {
      $client = new GuzzleHttp\Client();
      $response = $client->request('GET', 'https://tosdr.org/api/1/all.json');
      $json = $response->getBody();
      return json_decode($json, true);
    }
    
    public function detailAction(){
        $model = new Model_ModelService();
        $service = $model->getServiceById($_POST['idService']);        
        
        echo View_ServiceView::renderDetailServiceView($service);
    }
    
    public function readServiceAction(){
        $model = new Model_ModelService();
        $idService = $_POST['idService'];
        $idUser = $_SESSION['USER']->getId();
        $model->readService($idService,$idUser);
        
        echo 'ok';
    }
    
}
