<?php

class View_AdminView{
    public static function renderView($dprt, $service, $serviceRead){
        $html = "
            <div class='container-fluid'>
                <div class='row mt-3'>
                    <div class='col-lg-12'>
                        <div id='tabAdmin'>
                            <ul>
                                <li><a href='#tabs-1'>Departement</a></li>
                                <li><a href='#tabs-2'>Service</a></li>
                                <li><a href='#tabs-3'>Historique</a></li>
                            </ul>
                            <div id='tabs-1'>".
                                self::renderTabDprt($dprt)."
                            </div>
                            <div id='tabs-2'>".
                                self::renderTabService($service)."                            
                            </div>
                            <div id='tabs-3'>".
                                self::renderTabHisto($serviceRead)."                            
                            </div>
                        </div>
                    </div> 
                 </div>
             </div>
            <script>
                $(document).ready(function(){
                    $('#tabAdmin' ).tabs();
                });
            </script>
";
        return $html;
    }
    
    public static function renderTabDprt($dprt){
        $html = "<div id='accordionDprt'>";
        
        foreach($dprt->getListeDepartement() as $departement){
            $html .= "
                <h3>{$departement->getNom()}</h3>
                <div>
                    <table>";
            
            foreach($departement->getListUser() as $user){
                $html.="<tr>
                            <td>{$user->getNom()} {$user->getPrenom()}</td>
                            <td>Mentions lues : {$user->getNbMentionLues()}</td>
                            <td><form class='form-inline'>
                                <div class='form-group'>
                                    <select id='select' name='select' class='select form-control'>";
                foreach($dprt->getListeDepartement() as $o){
                    $selected = $o->getNom() == $user->getDepartement() ? 'selected' : '';
                    $html.="
                        <option value='{$o->getId()}' $selected>{$o->getNom()}</option>
                    ";
                }
                $html.="
                                    </select>
                            </div>
                            <btn data-id='{$user->getId()}' onclick='ajaxUpdateDprt(this)' class='ml-4 btn btn-primary'>Enregistrer</btn>
                            
                            </form></td>
                        </tr>";
            }
            
            $html .= "
                    </table>
                </div>";
        }
            
        $html .= "</div>
                    <script>
                        $( function() {
                            $( '#accordionDprt' ).accordion();
                        } );
                    </script>";
        return $html;
    }
    
    public static function renderTabService($listService){
        $html = "
            <table>";
        foreach($listService->getListeService() as $service){            
            $html.= "        
                <tr>
                    <td>{$service->getNom()}</td>
                    <td>{$service->getNbUserAgree()}</td>
                </tr>
            ";
        }
        $html.= "
            </table>
        ";
        
        return $html;
    }
    
    public static function renderTabHisto($serviceRead){
        $html = "
            <ul>";
        
        foreach($serviceRead->getListeServiceRead() as $mention){
            $html.="
                <li>{$mention->getNomUser()} - {$mention->getNomService()} - {$mention->getDateLecture()}</li>";
        }
        
        $html .= "
            </ul>
        ";
        
        return $html;
    }
}