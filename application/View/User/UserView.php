<?php

class View_UserView{
    
    public static function renderView(){
        $user = $_SESSION['USER'];
        $html = "
        <div class='container-fluid'>".
            self::renderForm().
            self::renderListService().
        "</div>        
        ";
        return $html;
    }
    
    public static function renderForm(){
        $user = $_SESSION['USER'];
        $html = "
            <div class='row mt-3'>
                <div class='col-lg-12'>
                    <div class='card'>
                        <h2 class='card-header'>Profil</h2>
                        <div class='row p-4'>
                            <div class='col-lg-8'>
                                <form method='post' action='/ril17-projet-web/User/update'>
                                    <div class='form-group'>
                                        <label for='NOM'>Nom : </label>
                                        <input type='text' class='form-control' name='NOM' value='{$user->getNom()}' required>
                                    </div>
                                    <div class='form-group'>
                                        <label for='PRENOM'>Prenom : </label>
                                        <input type='text' class='form-control' name='PRENOM' value='{$user->getPrenom()}' required>
                                    </div>
                                    <div class='form-group'>
                                        <label for='DT_NAIS'>Date de naissance : </label>
                                        <input type='text' pattern='(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}' placeholder='JJ/MM/AAAA' class='form-control' name='DT_NAIS' value='{$user->getDateNaissance()}' required>
                                    </div>
                                    <button type='submit' class='btn btn-primary'>Enregistrer</button>
                                </form>
                            </div>
                            <div class='col-lg-4'>
                                <p>Departement : {$user->getDepartement()}</p>
                                <p>Derniere connexion : {$user->getDateDerniereConnection()}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ";
        return $html;        
    }
    
    public static function renderListService(){
        $listeMention = $_SESSION['USER']->getMentionRead();
        $html="
            <div class='row mt-3'>
                <div class='col-lg-12'>
                    <div class='card'>
                        <h2 class='card-header'>Mentions lues</h2>
                        <div class='row p-4'>
                            <div class='col-lg-8'>
                                <ul>";
        foreach($listeMention as $mention){        
            $html.="
                                <li>{$mention->getNomService()} - {$mention->getDateLecture()}</li>";
        }
        
        $html.="
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ";
        return $html;
    }
    
}