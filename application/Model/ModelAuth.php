<?php

class Model_ModelAuth extends Model_Model
{
    public function getAllUsers(){
        $res = $this->db->query("
            SELECT
                *
            FROM T_user");
        
        $return = array();
        
        if($res->num_rows){
            $row = $res->fetch_all();
            
            foreach($row as $dataUsers){
                var_dump($row);
            }
        }
        return $return;
    }
    
    public function checkUser($pseudo, $pswd){
        $sql ="
            SELECT
                USER.ID_USER,
                USER.NOM,
                USER.PRENOM,
                USER.DT_NAIS,
                USER.DT_LAST_CO,
                USER.PSEUDO,
                ROLE.NAME ROLE,
                DPRT.NOM DPRT
            FROM T_USER USER
                INNER JOIN T_ROLE ROLE
                    ON ROLE.ID_ROLE = USER.FKID_ROLE
                INNER JOIN T_DPRT DPRT
                    ON DPRT.ID_DPRT = USER.FKID_DPRT
            WHERE 
                pseudo = ?
                AND PASSWORD = ?
            ";
      
        if($stmt = $this->db->prepare($sql)){
            $stmt->bind_param('ss', $pseudo, $pswd);
            
            $stmt->execute();           
            $res = $stmt->get_result();
            if($res->num_rows){
                $row = $res->fetch_assoc();
               
                $user = new Class_User($row['ID_USER']);
                $user   ->setNom($row['NOM'])
                        ->setPrenom($row['PRENOM'])
                        ->setDateDerniereConnection($row['DT_LAST_CO'])
                        ->setDateNaissance($row['DT_NAIS'])
                        ->setPseudo($row['PSEUDO'])
                        ->setRole($row['ROLE'])
                        ->setDepartement($row['DPRT']);
                
                $_SESSION['AUTH'] = true;
                $_SESSION['USER'] = $user;
                
                return true;
            }
            else{
                return false;
            }
        }        
    }    
}
